<?php

namespace Tests\Feature;

use App\Invoice;
use App\Domain\Enums\StatusEnum;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class InvoiceControllerTest extends TestCase
{
    use RefreshDatabase;

    // Test the Show Invoice endpoint
    public function testShowInvoice()
    {
        $invoice = Invoice::factory()->create();

        $response = $this->get("/api/invoices/{$invoice->id}");

        $response->assertStatus(200)
            ->assertJsonStructure([
                'id', 'number', 'date', 'due_date', 'company', 'billedCompany',
                'products', 'totalPrice', 'status',
            ]);
    }

    // Test the Approve Invoice endpoint
    public function testApproveInvoice()
    {
        $invoice = Invoice::factory()->create(['status' => StatusEnum::DRAFT]);

        $response = $this->post("/api/invoices/{$invoice->id}/approve"); // Use POST

        $response->assertStatus(200)
            ->assertJson(['message' => 'Invoice approved']);

    }

    // Test the Reject Invoice endpoint
    public function testRejectInvoice()
    {
        $invoice = Invoice::factory()->create(['status' => StatusEnum::DRAFT]);

        $response = $this->post("/api/invoices/{$invoice->id}/reject"); // Use POST

        $response->assertStatus(200)
            ->assertJson(['message' => 'Invoice rejected']);

    }
}
