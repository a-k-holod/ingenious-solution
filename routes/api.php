<?php


use Illuminate\Support\Facades\Route;

//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request): \Illuminate\Http\JsonResponse {
//    return response()->json($request->user());
//});

Route::get('/invoices/{id}', [InvoiceController::class, 'show']);
Route::post('/invoices/{id}/approve', [InvoiceController::class, 'approve']);
Route::post('/invoices/{id}/reject', [InvoiceController::class, 'reject']);
