<?php

namespace App\Domain\Invoices;

use App\Domain\Enums\StatusEnum;
use App\Domain\Invoices\Exception\CannotApproveException;
use App\Domain\Invoices\Exception\CannotRejectException;
use App\Domain\Invoices\ValueObjects\InvoiceId;
use DateTimeInterface;

class Invoice
{
    private InvoiceId $id;
    private string $number;
    private DateTimeInterface $date;
    private DateTimeInterface $dueDate;
    private StatusEnum $status;

    public function __construct(
        InvoiceId $id,
        string $number,
        DateTimeInterface $date,
        DateTimeInterface $dueDate,
        StatusEnum $status
    ) {
        // Initialize properties...
    }

    public function getId(): InvoiceId
    {
        return $this->id;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getDueDate(): DateTimeInterface
    {
        return $this->dueDate;
    }

    public function getStatus(): StatusEnum
    {
        return $this->status;
    }

    public function setStatus(StatusEnum $status): void
    {
        $this->status = $status;
    }

    public function isApproved(): bool
    {
        return StatusEnum::APPROVED === $this->status;
    }

    public function isRejected(): bool
    {
        return StatusEnum::REJECTED === $this->status;
    }

    // Implement other methods...
}
