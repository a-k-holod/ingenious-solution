<?php

declare(strict_types=1);

namespace App\Infrastructure\Repositories;

use App\Domain\Invoices\InvoiceRepository;
use App\Invoice;

class EloquentInvoiceRepository implements InvoiceRepository
{
    public function getById(int $id): ?Invoice
    {
        return Invoice::find($id);
    }

    public function save(Invoice $invoice): void
    {
        $invoice->save();
    }
}
