<?php

declare(strict_types=1);

namespace App\Domain\Invoices;

use App\Invoice;

interface InvoiceRepository
{
    public function getById(int $id): ?Invoice;

    public function save(Invoice $invoice): void;
}
