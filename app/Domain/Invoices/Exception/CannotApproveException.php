<?php


declare(strict_types=1);

namespace App\Domain\Invoices\Exception;

use RuntimeException;

class CannotApproveException extends RuntimeException
{
    protected $message = 'Cannot approve invoice.';
}
