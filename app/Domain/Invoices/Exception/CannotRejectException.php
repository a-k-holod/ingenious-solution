<?php

declare(strict_types=1);

namespace App\Domain\Invoices\Exception;

use RuntimeException;

class CannotRejectException extends RuntimeException
{
    protected $message = 'Cannot reject invoice.';
}
