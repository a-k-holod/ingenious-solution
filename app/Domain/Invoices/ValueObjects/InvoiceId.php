<?php
// app/Domain/Invoices/ValueObjects/InvoiceId.php

declare(strict_types=1);

namespace App\Domain\Invoices\ValueObjects;



class InvoiceId
{
    private UuidInterface $id;

    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }
}
