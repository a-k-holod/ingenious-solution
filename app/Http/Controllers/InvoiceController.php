<?php
// app/Http/Controllers/InvoiceController.php

use App\Domain\Enums\StatusEnum;
use App\Domain\Invoices\Exception\CannotApproveException;
use App\Domain\Invoices\Exception\CannotRejectException;
use App\Domain\Invoices\ValueObjects\InvoiceId;
use App\Invoice as InvoiceModel;
use App\Domain\Invoices\InvoiceRepository;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use Illuminate\Http\JsonResponse;

class InvoiceController extends Controller
{
    public function show(string $id, InvoiceRepository $invoiceRepository): JsonResponse
    {
        $invoiceId = new InvoiceId(Uuid::fromString($id));
        $invoice = $invoiceRepository->getById($invoiceId);

        if (!$invoice) {
            return response()->json(['message' => 'Invoice not found'], 404);
        }

        return response()->json($invoice);
    }

    public function approve(string $id, ApprovalFacadeInterface $approvalFacade, InvoiceRepository $invoiceRepository): JsonResponse
    {
        return $this->processApproval($id, $approvalFacade, $invoiceRepository, StatusEnum::APPROVED);
    }

    public function reject(string $id, ApprovalFacadeInterface $approvalFacade, InvoiceRepository $invoiceRepository): JsonResponse
    {
        return $this->processApproval($id, $approvalFacade, $invoiceRepository, StatusEnum::REJECTED);
    }

    private function processApproval(string $id, ApprovalFacadeInterface $approvalFacade, InvoiceRepository $invoiceRepository, StatusEnum $status): JsonResponse
    {
        $invoiceId = new InvoiceId(Uuid::fromString($id));
        $invoice = $invoiceRepository->getById($invoiceId);

        if (!$invoice) {
            return response()->json(['message' => 'Invoice not found'], 404);
        }

        try {
            if ($invoice->isApproved() || $invoice->isRejected()) {
                return response()->json(['message' => 'Invoice is already approved or rejected'], 400);
            }

            $entityClass = class_basename(InvoiceModel::class);

            $approvalDto = new ApprovalDto([
                'id' => $invoiceId->getId(),
                'status' => $status,
                'entity' => $entityClass,
            ]);

            $approvalFacade->approve($approvalDto);

            $invoice->setStatus($status);
            $invoiceRepository->save($invoice);

            $action = $status === StatusEnum::APPROVED ? 'approved' : 'rejected';
            return response()->json(['message' => "Invoice $action"]);
        } catch (CannotApproveException $approveException) {
            return response()->json(['message' => $approveException->getMessage()], 400);
        } catch (CannotRejectException $rejectException) {
            return response()->json(['message' => $rejectException->getMessage()], 400);
        }
    }

}
